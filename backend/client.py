import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
import django
django.setup()

from api.models import (
    Text, Channels, User, UserSocialJet
)

from telethon.sync import TelegramClient
from telethon import events
import telethon
import json
import telebot
import logging
from telethon.tl.functions.messages import GetHistoryRequest
from telethon.tl.functions.channels import GetParticipantsRequest
from telethon.tl.types import ChannelParticipantsSearch
from telethon.tl.functions.channels import JoinChannelRequest, LeaveChannelRequest
from telethon.tl.functions.messages import ImportChatInviteRequest
from asgiref.sync import sync_to_async

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')

# Подключение конфигурационного файла
with open('config/config.json', 'r', encoding='utf-8') as lib:
    data = json.load(lib)


@sync_to_async
def get_text(key: str) -> str:
    try:
        return Text.objects.get(key=key).text
    except Exception as e:
        logging.warning(f"key: {key}\nerror: {e}")


@sync_to_async
def add_channel(chat_id_channel_of_client: int, chat_id_channel: str, title: str,
                followers: int, join_link: str, chat_id_user: int):
    user = User.objects.get(chat_id=chat_id_user)
    Channels.objects.create(user=user, chat_id=chat_id_channel, chat_id_client=chat_id_channel_of_client,
                            title=title, followers=followers, join_link=join_link)


username = 'ronnycray'
api_id = 1911733
api_hash = '667148ea2a7da8ebdf1f484a66e4c81d'
chat_id_bot = 1553795848

token = data['token']
bot = telebot.TeleBot(token)

client = TelegramClient(session=username, api_id=api_id, api_hash=api_hash)


@client.on(events.NewMessage())
async def normal_handler(event):
    error = None
    try:
        chat_id = event.message.peer_id.user_id
    except Exception as e:
        chat_id = None
        error = e

    if chat_id is not None and chat_id == chat_id_bot:
        text_msg = event.message.message
        if 'НОВЫЙ КАНАЛ' in text_msg:
            lines = text_msg.split('\n')
            chat_id_user = lines[1].split('^')[1]
            chat_id_of_channel = lines.split('^')[1]
            count_followers = lines.split('^')[1]
            channel_link = lines.split('\n')[3]
            chat_id_channel = None
            chat_id_title = None
            if 'joinchat' in channel_link:
                if 'https' not in channel_link:
                    channel_link = channel_link.replace('http', 'https')
                channel = channel_link.split('https://t.me/joinchat/')[1]
                try:
                    updates = await client(ImportChatInviteRequest(hash=channel))
                    chat_id_channel = updates.chats[0].id
                    chat_id_title = updates.chats[0].title
                    logging.warning(updates)
                except Exception as e:
                    error = e
            else:
                if '@' in channel_link:
                    channel = f"@{channel_link.split('@')[1]}"
                else:
                    if 'https' not in channel_link:
                        channel_link = channel_link.replace('http', 'https')
                    channel = f"@{channel_link.split('https://t.me/')[1]}"

                try:
                    result = await client(JoinChannelRequest(channel=channel))
                    chat_id_channel = result.chats[0].id
                    chat_id_title = result.chats[0].title
                    logging.warning(result)
                except Exception as e:
                    error = e
            logging.warning(channel)

            if error is not None and chat_id_title is chat_id_title is not None and chat_id_channel is not None:
                bot.send_message(chat_id=chat_id_user,
                                 text=await get_text(key='you_channel_not_be_adding'))

            else:
                logging.warning(chat_id_of_channel)
                logging.warning(chat_id_channel)
                if f"-100{chat_id_channel}" == str(chat_id_of_channel):
                    await add_channel(chat_id_channel=chat_id_of_channel, chat_id_channel_of_client=chat_id_channel,
                                      chat_id_user=chat_id_user, followers=int(count_followers), join_link=channel,
                                      title=chat_id_title)
                    bot.send_message(chat_id=chat_id_user,
                                     text=await get_text(key='you_channel_successfully_adding'))


client.start()
client.run_until_disconnected()
