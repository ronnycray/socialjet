import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
import django
from django.core.files import File

django.setup()

from api.models import *
from api.models import (
    Text, Channels, User, UserSocialJet, Posts
)
from keyboards import (
    make_main_keyboard,
    make_keyboard_monitization,
    make_keyboard_manage_content,
    make_keyboard_manage_channels,
    make_keyboard_manage_post,
    make_cancel_keyboard,
    make_manage_my_channel_keyboard,
    make_edit_post_keyboard,
    make_cancel_of_create_post_keyboard
)

import requests
import logging
import hashlib
import telebot
import secrets
import json
import time
import sys
import re

# Подключение конфигурационного файла
with open('config/config.json', 'r', encoding='utf-8') as lib:
    data = json.load(lib)


limit_simple_text = 4096
limit_caption_media = 1024
limit_size_file_send = 50
limit_size_file_received = 20
limit_text_on_button = 200

size_name_file = 20

content_type_message = 'text'
content_type_photo = 'photo'
content_type_video = 'video'
content_type_audio = 'audio'
content_type_document = 'document'

path_to_media = '/Users/aleksandrchaika/Freelance/SocialJetBot/backend/media'

token = data['token']
bot = telebot.TeleBot(token)
social_jet_token = '7ASDna6sadgBS%#5&'
client_bot = 211176270

PATH_MAIN_FILE_PHOTO = data['PATH_MAIN_FILE_PHOTO']

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')

MENTION = 'mention'
""":obj:`str`: 'mention'"""
HASHTAG = 'hashtag'
""":obj:`str`: 'hashtag'"""
CASHTAG = 'cashtag'
""":obj:`str`: 'cashtag'"""
PHONE_NUMBER = 'phone_number'
""":obj:`str`: 'phone_number'"""
BOT_COMMAND = 'bot_command'
""":obj:`str`: 'bot_command'"""
URL = 'url'
""":obj:`str`: 'url'"""
EMAIL = 'email'
""":obj:`str`: 'email'"""
BOLD = 'bold'
""":obj:`str`: 'bold'"""
ITALIC = 'italic'
""":obj:`str`: 'italic'"""
CODE = 'code'
""":obj:`str`: 'code'"""
PRE = 'pre'
""":obj:`str`: 'pre'"""
TEXT_LINK = 'text_link'
""":obj:`str`: 'text_link'"""
TEXT_MENTION = 'text_mention'
""":obj:`str`: 'text_mention'"""
ALL_TYPES = [
    MENTION, HASHTAG, CASHTAG, PHONE_NUMBER, BOT_COMMAND, URL,
    EMAIL, BOLD, ITALIC, CODE, PRE, TEXT_LINK, TEXT_MENTION
]
"""List[:obj:`str`]: List of all the types."""


def get_text(key: str) -> str:
    try:
        return Text.objects.get(key=key).text
    except Exception as e:
        logging.warning(f"key: {key}\nerror: {e}")


menu_words_array = [get_text(key='manage_content'), get_text(key='monetization'),
                    get_text(key='manage_channels'), get_text(key='about_as'),
                    get_text(key='support_team'), get_text(key='back_main_menu'),
                    get_text(key='manage_post_btn'), get_text(key='manage_subscribe_btn'),
                    get_text(key='schedule_btn'), get_text(key='statistic_btn'),
                    get_text(key='settings_btn'), get_text(key='faq_btn'),
                    ]


@bot.message_handler(commands=['start'])
def get_start(message):
    print(message)
    chat_id = int(message.chat.id)
    user = User.objects.filter(chat_id=chat_id)

    if not user:
        print('chat_id:', chat_id)
        username = message.from_user.username if message.from_user.username else ""
        first_name = message.from_user.first_name if message.from_user.first_name else ""
        last_name = message.from_user.last_name if message.from_user.last_name else ""
        up_liner = 0

        if ' ' in message.text:
            user_up_liner = message.text.split('/start ')[1]
            if user_up_liner.isdigit():
                up_liner = user_up_liner

        User.objects.create(chat_id=chat_id, username=username, first_name=first_name, last_name=last_name,
                            up_liner=up_liner)

    bot.send_photo(chat_id=chat_id,
                   caption=get_text(key='main_text_message'),
                   parse_mode={'HTML'},
                   photo=open(PATH_MAIN_FILE_PHOTO, 'rb'),
                   reply_markup=make_main_keyboard())


@bot.message_handler(content_types='text')
def define_message(message):
    chat_id = message.chat.id
    print(message)
    user = User.objects.get(chat_id=message.chat.id)

    if message.text == get_text(key='manage_content'):
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='manage_content_text_menu'),
                         parse_mode={'HTML'},
                         reply_markup=make_keyboard_manage_content())

    elif message.text == get_text(key='monetization'):
        if is_auth_in_social_jet(user_object=user):
            token_sj = get_auth_token_user_social_jet(user_object=user)
            if token_sj != "":
                text = get_text(key='monitization_text_menu')
                keyboard = make_keyboard_monitization()
            else:
                text = get_text(key='you_not_register_or_account_not_found')
                keyboard = make_cancel_keyboard(message=message)
                bot.register_next_step_handler(message, get_email_user)
        else:
            text = get_text(key='you_need_auth_in_social_jet')
            keyboard = make_cancel_keyboard(message=message)
            bot.register_next_step_handler(message, get_email_user)

        bot.send_message(chat_id=chat_id,
                         text=text,
                         parse_mode={'HTML'},
                         reply_markup=keyboard)

    elif message.text == get_text(key='manage_channels'):
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='manage_channels_text_menu'),
                         parse_mode={'HTML'},
                         reply_markup=make_keyboard_manage_channels(message=message))

    elif message.text == get_text(key='about_as'):
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='about_us_text_menu'),
                         parse_mode={'HTML'})

    elif message.text == get_text(key='support_team'):
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='support_team_text_menu'),
                         parse_mode={'HTML'})

    elif message.text == get_text(key='back_main_menu'):
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='main_menu_text'),
                         parse_mode={'HTML'},
                         reply_markup=make_main_keyboard())

    elif message.text == get_text(key='manage_post_btn'):
        channels = Channels.objects.filter(user=user)
        if len(channels) != 0:
            text = get_text(key='manage_post_btn_text_menu')
        else:
            text = get_text(key='not_have_channel')

        bot.send_message(chat_id=chat_id,
                         text=text,
                         parse_mode={'HTML'},
                         reply_markup=make_keyboard_manage_post(message=message))

    elif message.text == get_text(key='manage_subscribe_btn'):
        pass

    elif message.text == get_text(key='schedule_btn'):
        if is_auth_in_social_jet(user_object=user) is False:
            text = get_text(key='you_need_auth_in_social_jet')
        else:
            text = get_text(key='schedule_text_menu')

        bot.send_message(chat_id=chat_id,
                         text=text,
                         parse_mode={'HTML'},
                         reply_markup=None)

    elif message.text == get_text(key='statistic_btn'):
        if is_auth_in_social_jet(user_object=user) is False:
            text = get_text(key='you_need_auth_in_social_jet')
        else:
            text = get_text(key='statistic_menu_text')

        bot.send_message(chat_id=chat_id,
                         text=text,
                         parse_mode={'HTML'},
                         reply_markup=None)

    elif message.text == get_text(key='settings_btn'):
        if is_auth_in_social_jet(user_object=user) is False:
            text = get_text(key='you_need_auth_in_social_jet')
        else:
            text = get_text(key='settings_menu_text')

        bot.send_message(chat_id=chat_id,
                         text=text,
                         parse_mode={'HTML'},
                         reply_markup=None)

    elif message.text == get_text(key='faq_btn'):
        if is_auth_in_social_jet(user_object=user) is False:
            text = get_text(key='you_need_auth_in_social_jet')
        else:
            text = get_text(key='faq_menu_text')

        bot.send_message(chat_id=chat_id,
                         text=text,
                         parse_mode={'HTML'},
                         reply_markup=None)


@bot.callback_query_handler(func=lambda call_back: True)
def define_callback(call_back):
    print(call_back.data)
    chat_id = int(call_back.message.chat.id)
    message = call_back.message
    bot.clear_step_handler(message=message)
    user = User.objects.get(chat_id=message.chat.id)

    if 'show_info_my_channel' in call_back.data:
        pass

    elif 'manage_content_my_channel' in call_back.data:
        id_channel = int(call_back.data.split('-')[1])
        channel = Channels.objects.get(pk=id_channel)
        bot.edit_message_text(chat_id=chat_id,
                              message_id=message.message_id,
                              text=get_text(key='text_manage_my_channel').format(channel.title),
                              parse_mode={'HTML'},
                              reply_markup=make_manage_my_channel_keyboard(channel=channel))

    elif 'back_to_show_my_channels' in call_back.data:
        channels = Channels.objects.filter(user=user)
        if len(channels) != 0:
            text = get_text(key='manage_post_btn_text_menu')
        else:
            text = get_text(key='not_have_channel')

        bot.edit_message_text(chat_id=chat_id,
                              message_id=message.message_id,
                              text=text,
                              parse_mode={'HTML'},
                              reply_markup=make_keyboard_manage_post(message=message))

    elif 'create_post_in_channel' in call_back.data:
        id_channel = int(call_back.data.split('-')[1])
        channel = Channels.objects.get(pk=id_channel)
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='send_me_post_for_edit').format(channel.title),
                         parse_mode={"HTML"},
                         reply_markup=make_cancel_of_create_post_keyboard())
        bot.register_next_step_handler(message, get_post_user, channel)

    elif 'cancel_create_in_channel_post' == call_back.data:
        bot.clear_step_handler(message=message)
        channels = Channels.objects.filter(user=user)
        if len(channels) != 0:
            text = get_text(key='manage_post_btn_text_menu')
        else:
            text = get_text(key='not_have_channel')

        bot.send_message(chat_id=chat_id,
                         text=text,
                         parse_mode={'HTML'},
                         reply_markup=make_keyboard_manage_post(message=message))

    elif 'show_posts_in_channel' in call_back.data:
        id_channel = int(call_back.data.split('-')[1])
        channel = Channels.objects.get(pk=id_channel)

    elif 'add_channel_user' == call_back.data:
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='send_message_from_channel_for_adding'),
                         reply_markup=make_cancel_keyboard(message=message),
                         parse_mode={'HTML'})
        bot.register_next_step_handler(message, add_channel)

    elif 'show_my_posts_channel' in call_back.data:
        pass

    bot.answer_callback_query(callback_query_id=call_back.id)


def get_post_user(message, channel):
    bot.clear_step_handler(message=message)
    content_type = message.content_type
    caption = message.text
    if caption is None:
        caption = message.caption
    else:
        if caption in menu_words_array:
            define_message(message=message)
            return None

    caption = _parse_markdown(message=message, message_text=caption)
    print('caption', caption)

    try:
        msg_info = bot.send_message(chat_id=client_bot,
                                    text=caption,
                                    parse_mode={'markdown'})
        caption = msg_info.text
    except telebot.apihelper.ApiException as e:
        print(e)
        msg_info = bot.send_message(chat_id=client_bot,
                                    text=caption,
                                    parse_mode={'HTML'})
        caption = msg_info.text

    caption = _parse_html(message=msg_info, message_text=caption)

    if content_type == 'video':
        video_file = bot.get_file(message.video.file_id)
        downloaded_file = bot.download_file(video_file.file_path)

        media = f'{path_to_media}/{secrets.token_hex(size_name_file)}-{message.video.file_id}.mp4'
        with open(media, 'wb') as new_file:
            new_file.write(downloaded_file)
    elif content_type == 'photo':
        photo = bot.get_file(message.photo[-1].file_id)
        downloaded_file = bot.download_file(photo.file_path)
        media = f'{path_to_media}/{secrets.token_hex(size_name_file)}.jpg'
        while os.path.exists(media):
            media = f'{path_to_media}/{secrets.token_hex(size_name_file)}.jpg'
        with open(media, 'wb') as new_file:
            new_file.write(downloaded_file)
    elif content_type == 'document':
        doc = bot.get_file(message.document.file_id)
        downloaded_file = bot.download_file(doc.file_path)
        format_file = message.document.mime_type.split('/')[1]
        media = f'{path_to_media}/{secrets.token_hex(size_name_file)}.{format_file}'
        while os.path.exists(media):
            media = f'{path_to_media}/{secrets.token_hex(size_name_file)}.{format_file}'
        with open(media, 'wb') as new_file:
            new_file.write(downloaded_file)
    else:
        media = ""

    user = User.objects.get(chat_id=message.chat.id)
    if media != "":
        media_file = open(media, 'rb')
        form = File(media_file)
        new_post = Posts.objects.create(channel=channel, creator=user, content_type=content_type,
                                        media=form, caption=caption)
    else:
        new_post = Posts.objects.create(channel=channel, creator=user, content_type=content_type, caption=caption)

    send_post_user(message=message, post=new_post)


def send_post_user(message, post):
    if post.media != "":
        print(post.media.path)
        media = open(post.media.path, 'wb')
    else:
        media = None
    # keyboard = make_edit_post_keyboard(post=post)
    keyboard = None
    if post.content_type == 'video':
        bot.send_video(chat_id=message.chat.id,
                       data=media,
                       caption=post.caption,
                       parse_mode={'HTML'},
                       disable_notification=False,
                       reply_markup=keyboard)
    elif post.content_type == 'photo':
        bot.send_photo(chat_id=message.chat.id,
                       photo=media,
                       caption=post.caption,
                       parse_mode={'HTML'},
                       disable_notification=False,
                       reply_markup=keyboard)
    elif post.content_type == 'document':
        bot.send_document(chat_id=message.chat.id,
                          data=media,
                          caption=post.caption,
                          parse_mode={'HTML'},
                          disable_notification=False,
                          reply_markup=keyboard)
    else:
        bot.send_message(chat_id=message.chat.id,
                         text=post.caption,
                         parse_mode={'HTML'},
                         disable_notification=False,
                         reply_markup=keyboard)


def get_email_user(message):
    if message.content_type == 'text':
        if message.text == get_text(key='cancel'):
            send_main_menu(message)
        else:
            email = message.text
            user_object = User.objects.get(chat_id=message.chat.id)
            user_object.email = email
            user_object.save()
            token_sj = get_auth_token_user_social_jet(user_object=user_object)
            if token_sj != "":
                user_object.token_sj = token_sj
                user_object.save()
                bot.send_message(chat_id=message.chat.id,
                                 text=get_text(key='monitization_text_menu'),
                                 parse_mode={'HTML'},
                                 reply_markup=make_keyboard_monitization())
            else:
                bot.send_message(chat_id=message.chat.id,
                                 text=get_text(key='you_not_register_or_account_not_found'),
                                 parse_mode={'HTML'},
                                 reply_markup=make_cancel_keyboard(message=message))
                bot.register_next_step_handler(message, get_email_user)
    else:
        bot.send_message(chat_id=message.chat.id,
                         text=get_text(key='you_send_not_email_try_again_msg_type'),
                         reply_markup=make_cancel_keyboard(message=message),
                         parse_mode={'HTML'})
        bot.register_next_step_handler(message, get_email_user)


def send_main_menu(message):
    bot.send_photo(chat_id=message.chat.id,
                   caption=get_text(key='main_text_message'),
                   parse_mode={'HTML'},
                   photo=open(PATH_MAIN_FILE_PHOTO, 'rb'),
                   reply_markup=make_main_keyboard())


def add_channel(message):
    cancel = False
    if message.text is not None:
        if message.text == get_text(key='cancel'):
            send_main_menu(message)
            cancel = True

    if cancel is False:
        if message.forward_from_chat is not None:
            if message.forward_from_chat.type == 'channel':
                chat_id_channel = message.forward_from_chat.id
                channel = Channels.objects.filter(chat_id=chat_id_channel)
                if not channel:
                    try:
                        admins = bot.get_chat_administrators(chat_id=chat_id_channel)
                    except telebot.apihelper.ApiException as e:
                        print(e)
                        admins = []
                    print(admins)
                    adder = None
                    if len(admins) != 0:
                        for admin in admins:
                            if admin.user.id == message.chat.id:
                                print(admin)
                                adder = admin
                                break

                        if adder is not None:
                            print(adder)
                            if adder.status == 'creator':
                                title_channel = message.forward_from_chat.title
                                bot.send_message(chat_id=message.chat.id,
                                                 text=get_text(key='send_me_join_link_on_channel'),
                                                 reply_markup=make_cancel_keyboard(message=message),
                                                 parse_mode={'HTML'})
                                bot.register_next_step_handler(message, get_join_link_channel, chat_id_channel,
                                                               title_channel)
                            else:
                                bot.send_message(chat_id=message.chat.id,
                                                 text=get_text(key='error_you_not_creator_of_channel'),
                                                 parse_mode={'HTML'},
                                                 reply_markup=make_cancel_keyboard(message=message))
                                bot.register_next_step_handler(message, add_channel)
                        else:
                            bot.send_message(chat_id=message.chat.id,
                                             text=get_text(key='error_you_not_member_admins_of_channel'),
                                             parse_mode={'HTML'},
                                             reply_markup=make_cancel_keyboard(message=message))
                            bot.register_next_step_handler(message, add_channel)
                    else:
                        bot.send_message(chat_id=message.chat.id,
                                         text=get_text(key='error_bot_is_not_in_admins_of_channel'),
                                         parse_mode={'HTML'},
                                         reply_markup=make_cancel_keyboard(message=message))
                        bot.register_next_step_handler(message, add_channel)
                else:
                    bot.send_message(chat_id=message.chat.id,
                                     text=get_text(key='error_channel_is_have'),
                                     parse_mode={'HTML'},
                                     reply_markup=make_cancel_keyboard(message=message))
                    bot.register_next_step_handler(message, add_channel)
            else:
                bot.send_message(chat_id=message.chat.id,
                                 text=get_text(key='error_type_chat_of_adding_channel'),
                                 parse_mode={'HTML'},
                                 reply_markup=make_cancel_keyboard(message=message))
                bot.register_next_step_handler(message, add_channel)
        else:
            bot.send_message(chat_id=message.chat.id,
                             text=get_text(key='error_type_frw_msg_of_adding_channel'),
                             parse_mode={'HTML'},
                             reply_markup=make_cancel_keyboard(message=message))
            bot.register_next_step_handler(message, add_channel)


def get_join_link_channel(message, chat_id_channel, title_channel):
    if message.text is not None:
        if message.text == get_text(key='cancel'):
            send_main_menu(message)
        else:
            link = message.text
            if 't.me' in link:
                try:
                    count = bot.get_chat_members_count(chat_id=chat_id_channel)
                    bot.send_message(chat_id=client_bot,
                                     text=f"chat_id_channel^{chat_id_channel}\n"
                                     f"chat_id_user^{message.chat.id}\n"
                                     f"count^{count}\n"
                                     f"{link}\n\n"
                                     f"НОВЫЙ КАНАЛ",
                                     disable_web_page_preview=False)
                except telebot.apihelper.ApiException as e:
                    logging.warning(e)
                    bot.send_message(chat_id=message.chat.id,
                                     text=get_text(key='error_bot_is_not_in_admins_of_channel'),
                                     parse_mode={'HTML'},
                                     reply_markup=make_cancel_keyboard(message=message))
                    bot.register_next_step_handler(message, get_join_link_channel, chat_id_channel,
                                                   title_channel)
            else:
                bot.send_message(chat_id=message.chat.id,
                                 text=get_text(key='you_send_me_link_on_not_channel'))
                bot.register_next_step_handler(message, get_join_link_channel, chat_id_channel,
                                               title_channel)
    else:
        bot.send_message(chat_id=message.chat.id,
                         text=get_text(key='you_send_me_not_link'),
                         reply_markup=make_cancel_keyboard(message=message))
        bot.register_next_step_handler(message, get_join_link_channel, chat_id_channel,
                                       title_channel)


def is_auth_in_social_jet(user_object: User):
    if user_object.email is not None:
        return True
    return False


def get_auth_token_user_social_jet(user_object: User):
    time_now = int(time.time())
    url_auth = 'https://socialjet.pro/index.php?r=api/site'
    s = f"{user_object.email}:{social_jet_token}:{time_now}"
    signature = hashlib.md5(s.encode()).hexdigest()
    data_json = {
        "jsonrpc": "2.0",
        "method": "tgLogin",
        "params": {
            "siteId": 1,
            "email": user_object.email,
            "token": signature,
            "time": time_now
        },
        "id": 0
    }
    try:
        response = requests.post(url=url_auth, json=data_json).json()
        if response.get("error", False):
            return ""
        return response['result']
    except Exception as e:
        print(e)


def get_schedule_of_user(user_object: User, chat_id_channel: str):
    time_now = int(time.time())
    url_auth = 'https://socialjet.pro/index.php?r=api/site'
    s = f"{user_object.email}:{social_jet_token}:{time_now}"
    signature = hashlib.md5(s.encode()).hexdigest()
    data_json = {
        "jsonrpc": "2.0",
        "method": "getSchedule",
        "params": {
            "groupId": chat_id_channel,
            "email": user_object.email,
            "token": signature,
            "time": time_now
        },
        "id": 0
    }


def get_group_list(user: User):
    url = 'https://socialjet.pro/index.php?r=api/site'
    data_json = {
        "jsonrpc": "2.0",
        "method": "getGroupList",
        "params": {
            "siteId": 1,
            "auth": user.token_sj,
        },
        "id": 0
    }
    resp = requests.post(url=url, json=data_json).json()
    channels_user = []
    for channel in resp['result']:
        channels_user.append({
            "title": channel['name'],
            "id_sj": channel["id"]
        })


def parse_entities(entities, text, types=None):
    if types is None:
        types = ALL_TYPES

    list_entity = []
    cnt = 0

    for entity in entities:
        if entity['type'] in types:
            entity['text'] = parse_entity(entity, text)
            list_entity.append(entity)
            cnt += 1

    return list_entity


def parse_entity(entity, text):
    if sys.maxunicode == 0xffff:
        return text[entity['offset']:entity['offset'] + entity['length']]
    else:
        entity_text = text.encode('utf-16-le')
        entity_text = entity_text[entity['offset'] * 2:(entity['offset'] + entity['length']) * 2]

        return entity_text.decode('utf-16-le')


def escape_markdown(text):
    """Helper function to escape telegram markup symbols."""
    escape_chars = '\*_`\['
    return re.sub(r'([%s])' % escape_chars, r'\\\1', text)


def _parse_html(message, message_text, urled=False):
    if message_text is None and message is None:
        return None

    if message.content_type == 'text':
        entities = message.json.get('entities', None)
    else:
        entities = message.json.get('caption_entities', None)

    print('message_text', message_text)

    if entities is not None:
        entities = parse_entities(entities=entities, text=message_text)

    if not sys.maxunicode == 0xffff:
        message_text = message_text.encode('utf-16-le')

    markdown_text = ''
    last_offset = 0

    if entities is not None:

        for element in entities:
            # text = escape_markdown(element['text'])
            text = element['text']
            print(text)

            if element['type'] == 'text_link':
                insert = '<a href="{}">{}</a>'.format(element['url'], text)
            elif element['type'] == 'text_link' and element['user']:
                insert = '<a href="tg://user?id={}">{}</a>'.format(element['user']['id'], text)
            elif element['type'] == 'url' and urled:
                insert = '<a href="{0}">{0}</a>'.format(text)
            elif element['type'] == 'bold':
                insert = '<b>' + text + '</b>'
            elif element['type'] == 'italic':
                insert = '<i>' + text + '</i>'
            elif element['type'] == 'code':
                insert = '<code>' + text + '</code>'
            elif element['type'] == 'pre':
                insert = '```' + text + '```'
            else:
                insert = text
            if sys.maxunicode == 0xffff:
                markdown_text += escape_markdown(message_text[last_offset:element['offset']]) + insert
            else:
                markdown_text += escape_markdown(
                    message_text[last_offset * 2:element['offset'] * 2].decode('utf-16-le')) + insert

            last_offset = element['offset'] + element['length']

    if sys.maxunicode == 0xffff:
        markdown_text += escape_markdown(message_text[last_offset:])
    else:
        markdown_text += escape_markdown(message_text[last_offset * 2:].decode('utf-16-le'))
    return markdown_text


def _parse_markdown(message, message_text, urled=False):
    if message_text is None and message is None:
        return None

    if message.content_type == 'text':
        entities = message.json.get('entities', None)
    else:
        entities = message.json.get('caption_entities', None)

    if entities is not None:
        entities = parse_entities(entities=entities, text=message_text)

    if not sys.maxunicode == 0xffff:
        message_text = message_text.encode('utf-16-le')

    markdown_text = ''
    last_offset = 0

    if entities is not None:

        for element in entities:
            text = escape_markdown(element['text'])
            print('text', text)

            if element['type'] == 'text_link':
                insert = '[{}]({})'.format(text, element['url'])
            elif element['type'] == 'text_link' and element['user']:
                insert = '[{}](tg://user?id={})'.format(text, element['user']['id'])
            elif element['type'] == 'url' and urled:
                insert = '[{0}]({0})'.format(text)
            elif element['type'] == 'bold':
                insert = '*' + text + '*'
            elif element['type'] == 'italic':
                insert = '_' + text + '_'
            elif element['type'] == 'code':
                insert = '`' + text + '`'
            elif element['type'] == 'pre':
                insert = '```' + text + '```'
            else:
                insert = text
            print('insert', insert)
            if sys.maxunicode == 0xffff:
                markdown_text += escape_markdown(message_text[last_offset:element['offset']]) + insert
            else:
                markdown_text += escape_markdown(
                    message_text[last_offset * 2:element['offset'] * 2].decode('utf-16-le')) + insert

            last_offset = element['offset'] + element['length']

    if sys.maxunicode == 0xffff:
        markdown_text += escape_markdown(message_text[last_offset:])
    else:
        markdown_text += escape_markdown(message_text[last_offset * 2:].decode('utf-16-le'))

    print('pre', markdown_text)
    markdown_text = markdown_text.replace('\*', '*').replace('\_', "_").replace('\[', '[')
    print('post', markdown_text)

    return markdown_text


bot.polling(none_stop=True)
