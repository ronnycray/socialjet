from django.urls import path

from .views import *

urlpatterns = [
    path('is_have_user/', is_have_user),
    path('get_up_liner_of_user/', get_up_liner_of_user),
    path('get_text/', get_text),
    path('get_menu/', get_menu),
    path('create_user/', create_user),
    path('get_channels_of_user/', get_channels_of_user)
]
