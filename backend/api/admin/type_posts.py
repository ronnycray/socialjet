from django.contrib import admin
from ..models.type_post import TypePost


@admin.register(TypePost)
class TypePostAdmin(admin.ModelAdmin):
    list_display = ("name", "describe", "allowed",)
    list_display_links = ("name", "describe", "allowed",)
