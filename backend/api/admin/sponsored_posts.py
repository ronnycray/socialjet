from django.contrib import admin
from ..models.sponsored_posts import SponsoredPosts


@admin.register(SponsoredPosts)
class SponsoredPostsAdmin(admin.ModelAdmin):
    list_display = ("id", "name_post", "views", "busy",)
    list_display_links = ("id", "name_post", "views", "busy",)
