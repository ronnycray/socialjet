from django.contrib import admin
from ..models.faq import FAQ


@admin.register(FAQ)
class FAQAdmin(admin.ModelAdmin):
    list_display = ("question",)
    list_display_links = ("question",)
