from django.contrib import admin
from ..models.requisites import Requisites


@admin.register(Requisites)
class RequisitesAdmin(admin.ModelAdmin):
    list_display = ("user", "number_cart", "describe",)
    list_display_links = ("user", "number_cart", "describe",)
