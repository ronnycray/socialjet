from django.contrib import admin
from ..models.invoices import Invoices


@admin.register(Invoices)
class InvoicesAdmin(admin.ModelAdmin):
    list_display = ("user", "type_follow", "date_end_follow",)
    list_display_links = ("user", "type_follow", "date_end_follow",)
