from django.contrib import admin
from ..models.buttons import Buttons


@admin.register(Buttons)
class ButtonsAdmin(admin.ModelAdmin):
    list_display = ("id", "post", "text", "click")
    list_display_links = ("id", "post", "text", "click")
