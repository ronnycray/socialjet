from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.utils.safestring import mark_safe
from django.contrib import admin
from ..models.text import Text
from django import forms


class TextAdminForm(forms.ModelForm):
    text = forms.CharField(label="Значение", widget=CKEditorUploadingWidget())

    class Meta:
        model = Text
        fields = '__all__'


@admin.register(Text)
class AdminText(admin.ModelAdmin):
    list_display_links = ("key", "text",)
    list_display = ("key", "text",)
