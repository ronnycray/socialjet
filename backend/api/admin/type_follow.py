from django.contrib import admin
from ..models.type_follow import TypeFollow


@admin.register(TypeFollow)
class TypeFollowAdmin(admin.ModelAdmin):
    list_display = ("name", "price", "day", "discount",)
    list_display_links = ("name", "price", "day", "discount",)
