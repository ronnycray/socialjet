from django.contrib import admin
from ..models.channels import Channels


@admin.register(Channels)
class ChannelsAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "user", "create_at")
    list_display_links = ("id", "title", "user", "create_at")
