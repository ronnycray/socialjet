from django.contrib import admin
from ..models.posts import Posts


@admin.register(Posts)
class PostsAdmin(admin.ModelAdmin):
    list_display = ("channel", "creator", "views", "create_at",)
    list_display_links = ("channel", "creator", "views", "create_at",)
