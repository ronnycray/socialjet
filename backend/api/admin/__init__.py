from .text import AdminText
from .user import UserAdmin
from .bots import BotsAdmin
from .channels import ChannelsAdmin
from .buttons import ButtonsAdmin
from .categories import CategoriesAdmin
from .coupons import CouponsAdmin
from .faq import FAQAdmin
from .invoices import InvoicesAdmin
from .posts import PostsAdmin
from .requisites import RequisitesAdmin
from .sponsored_posts import SponsoredPostsAdmin
from .type_follow import TypeFollowAdmin
from .type_posts import TypePostAdmin

from django.contrib import admin

admin.site.site_header = 'SocialJet Административная Панель'
admin.site.site_title = 'Админ SocialJet'
