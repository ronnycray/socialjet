from django.contrib import admin
from ..models.bots import Bots


@admin.register(Bots)
class BotsAdmin(admin.ModelAdmin):
    list_display = ("id", "user", "api_token", "create_at")
    list_display_links = ("id", "user", "api_token", "create_at")
