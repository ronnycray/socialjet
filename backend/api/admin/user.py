from django.utils.safestring import mark_safe
from django.contrib import admin
from ..models.user import User
from ..models.channels import Channels


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display_links = ("id", "username", "chat_id", "score")
    list_display = ("id", "username", "chat_id", "score")
    readonly_fields = ("get_up_liner", "get_channels")
    exclude = ("up_liner",)

    def get_up_liner(self, obj):
        up_liner = User.objects.filter(pk=obj.up_liner)
        if up_liner:
            info_up_liner = up_liner[0]
            out = f'<a href="/admin/api/user/{info_up_liner.pk}/change/"><b>{info_up_liner.username}<b></a>'
        else:
            out = f"Отсутствует"
        return mark_safe(out)

    def get_channels(self, obj):
        channels = Channels.objects.filter(user=obj)
        if len(channels) != 0:
            out = "<ul>"
            for channel in channels:
                out += f'<li><a href=""><b>{channel.name}</b></a></li>'
            out += "<ul>"
        else:
            out = "-"

        return  mark_safe(out)

    get_up_liner.short_description = "Пригласил"
    get_channels.short_description = "Каналы"
