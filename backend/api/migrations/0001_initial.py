# Generated by Django 3.1.7 on 2021-02-20 17:19

import api.models.range_field
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categories',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=100, verbose_name='Название категории')),
            ],
            options={
                'verbose_name': 'Категория',
                'verbose_name_plural': 'Категории',
            },
        ),
        migrations.CreateModel(
            name='Channels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chat_id', models.CharField(default='', max_length=100, verbose_name='Chat ID')),
                ('chat_id_client', models.IntegerField(default=0, verbose_name='Chat ID Client')),
                ('title', models.CharField(default='', max_length=100, verbose_name='Название')),
                ('followers', models.IntegerField(default=0, verbose_name='Подписчиков')),
                ('join_link', models.CharField(default='', max_length=100, verbose_name='Пригласительная ссылка')),
                ('create_at', models.DateTimeField(auto_now_add=True, verbose_name='Create at')),
                ('categories', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.categories', verbose_name='Категории канала')),
            ],
            options={
                'verbose_name': 'Канал',
                'verbose_name_plural': 'Каналы',
            },
        ),
        migrations.CreateModel(
            name='Coupons',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=10, verbose_name='Название купона')),
                ('discount', models.IntegerField(default=0, verbose_name='Скидка в %')),
                ('amount', models.IntegerField(default=0, verbose_name='Скидка в руб.')),
            ],
            options={
                'verbose_name': 'Купон',
                'verbose_name_plural': 'Купоны',
            },
        ),
        migrations.CreateModel(
            name='FAQ',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(default='', max_length=100, verbose_name='Вопрос')),
                ('answer', models.TextField(default='', max_length=200, verbose_name='Ответ')),
            ],
            options={
                'verbose_name': 'Часто задаваемый вопрос',
                'verbose_name_plural': 'Часто задаваемые вопросы',
            },
        ),
        migrations.CreateModel(
            name='Text',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.CharField(default='', max_length=100, verbose_name='Ключ')),
                ('text', models.TextField(default='', max_length=1000, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Текст',
                'verbose_name_plural': 'Текста',
            },
        ),
        migrations.CreateModel(
            name='TypeFollow',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=100, verbose_name='Имя тарифа')),
                ('price', models.IntegerField(default=0, verbose_name='Цена')),
                ('day', models.IntegerField(default=0, verbose_name='Дней')),
                ('discount', models.IntegerField(default=0, verbose_name='Скидка в %')),
            ],
            options={
                'verbose_name': 'Тариф',
                'verbose_name_plural': 'Тарифы',
            },
        ),
        migrations.CreateModel(
            name='TypePost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=20, verbose_name='Тип')),
                ('describe', models.CharField(default='', max_length=200, verbose_name='Описание')),
                ('allowed', models.BooleanField(default=True, verbose_name='Доступен')),
            ],
            options={
                'verbose_name': 'Тип поста',
                'verbose_name_plural': 'Типы постов',
            },
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chat_id', models.IntegerField(default=0, verbose_name='Chat ID')),
                ('username', models.CharField(default='', max_length=100, verbose_name='Username')),
                ('first_name', models.CharField(default='', max_length=100, verbose_name='First Name')),
                ('last_name', models.CharField(default='', max_length=100, verbose_name='Last Name')),
                ('create_at', models.DateTimeField(auto_now_add=True, verbose_name='Create at')),
                ('last_update', models.DateTimeField(auto_now_add=True, verbose_name='Last update')),
                ('score', models.IntegerField(default=500, verbose_name='Очков рейтинга')),
            ],
            options={
                'verbose_name': 'Пользвотель',
                'verbose_name_plural': 'Пользователи',
            },
        ),
        migrations.CreateModel(
            name='SponsoredPosts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('caption', models.TextField(default='', max_length=1024, verbose_name='Текст')),
                ('photo', models.ImageField(blank=True, default='', upload_to='media/', verbose_name='Картинка')),
                ('video', models.FileField(blank=True, default='', upload_to='media/', verbose_name='Видео')),
                ('document', models.ImageField(blank=True, default='', upload_to='media/', verbose_name='Документ')),
                ('buttons', models.CharField(default='', max_length=1000, verbose_name='Кнопки')),
                ('views', models.IntegerField(default=0, verbose_name='Охват')),
                ('price', models.IntegerField(default=0, verbose_name='Оплата')),
                ('busy', models.BooleanField(default=False, verbose_name='Найден исполнитель')),
                ('categories', models.ManyToManyField(to='api.Categories')),
            ],
            options={
                'verbose_name': 'Рекламный пост SocialJet',
                'verbose_name_plural': 'Рекламные посты SocialJet',
            },
        ),
        migrations.CreateModel(
            name='Requisites',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number_cart', models.CharField(default='', max_length=50, verbose_name='Реквизиты')),
                ('describe', models.CharField(default='', max_length=200, verbose_name='Комментарий')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.user', verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Реквизиты',
                'verbose_name_plural': 'Реквизиты',
            },
        ),
        migrations.CreateModel(
            name='Posts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content_type', models.CharField(default='text', max_length=100, verbose_name='Тип контента')),
                ('media', models.FileField(blank=True, default='', max_length=1000, upload_to='media/', verbose_name='Файл')),
                ('caption', models.TextField(default='', max_length=4096, verbose_name='Текст')),
                ('message_id', models.IntegerField(default=0, verbose_name='Message ID')),
                ('views', models.IntegerField(default=0, verbose_name='Просмотров')),
                ('create_at', models.DateTimeField(auto_now_add=True, verbose_name='Создано в')),
                ('date_send', models.DateTimeField(verbose_name='Отправить в')),
                ('send', models.BooleanField(default=False, verbose_name='Отправлено')),
                ('channel', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.channels', verbose_name='Канал')),
                ('creator', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.user', verbose_name='Создатель')),
                ('type_post', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.typepost', verbose_name='Тип поста')),
            ],
            options={
                'verbose_name': 'Пост',
                'verbose_name_plural': 'Посты',
            },
        ),
        migrations.CreateModel(
            name='Invoices',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('buy_at', models.DateTimeField(auto_now_add=True, verbose_name='Куплена')),
                ('date_end_follow', models.DateTimeField(verbose_name='Заканчивается')),
                ('before_day_notify', models.BooleanField(default=False, verbose_name='Уведоление за день до кончания')),
                ('active', models.BooleanField(default=True, verbose_name='Активная подписка')),
                ('type_follow', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.typefollow', verbose_name='Тариф')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.user', verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Подписка',
                'verbose_name_plural': 'Подписки',
            },
        ),
        migrations.AddField(
            model_name='channels',
            name='user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.user', verbose_name='User'),
        ),
        migrations.CreateModel(
            name='CalendarUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_send', models.DateTimeField(verbose_name='Дата')),
                ('busy', models.BooleanField(default=False, verbose_name='День занят')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.user', verbose_name='Пользователь')),
            ],
        ),
        migrations.CreateModel(
            name='Buttons',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(default='', max_length=50, verbose_name='Текст')),
                ('url', models.CharField(default='', max_length=500, verbose_name='URL')),
                ('answer', models.TextField(default='', max_length=200, verbose_name='Ответ')),
                ('click', models.IntegerField(default=0, verbose_name='Кликов')),
                ('post', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.posts', verbose_name='Пост')),
            ],
            options={
                'verbose_name': 'Кнопка',
                'verbose_name_plural': 'Кнопки',
            },
        ),
        migrations.CreateModel(
            name='Bots',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('api_token', models.CharField(default='', max_length=300, verbose_name='Token')),
                ('create_at', models.DateTimeField(auto_now_add=True, verbose_name='Create at')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.user', verbose_name='User')),
            ],
            options={
                'verbose_name': 'Бот',
                'verbose_name_plural': 'Боты',
            },
        ),
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('level', api.models.range_field.IntegerRangeField(verbose_name='Level')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='api.user', verbose_name='User')),
            ],
            options={
                'verbose_name': 'Админ',
                'verbose_name_plural': 'Админы',
            },
        ),
    ]
