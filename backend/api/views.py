from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.shortcuts import render
from .models import *
import json


def get_body(request):
    payloads = json.loads(request.body)
    print(payloads)
    return payloads


@csrf_exempt
def is_have_user(request):
    body = get_body(request=request)
    chat_id = body.get('chat_id', None)
    print(chat_id)
    have = False
    if chat_id is not None:
        user_object = User.objects.filter(chat_id=chat_id)
        print(user_object)
        if user_object:
            have = True
    print(have)

    return JsonResponse(data={"have": have})


@csrf_exempt
def get_up_liner_of_user(request):
    body = get_body(request=request)
    chat_id = body.get('chat_id', None)
    response = {
        "chat_id": None,
        "username": None,
        "name": None
    }
    if chat_id is not None:
        id_up_liner = User.objects.get(chat_id=chat_id).up_liner
        up_liner = User.objects.filter(pk=id_up_liner)
        if up_liner:
            response["chat_id"] = up_liner[0].chat_id
            response["username"] = up_liner[0].username
            response["name"] = f"{up_liner[0].first_name} {up_liner[0].last_name}"

    return JsonResponse(data=response)


@csrf_exempt
def create_user(request):
    body = get_body(request=request)
    print(body)
    chat_id = body.get('chat_id', None)
    username = body.get('username', None)
    first_name = body.get('first_name', None)
    last_name = body.get('last_name', None)
    up_liner = body.get('up_liner', None)
    status = False
    if chat_id is not None and username is not None and first_name is not None and last_name is not None and \
            up_liner is not None:
        is_up_liner = User.objects.filter(chat_id=up_liner)
        print(is_up_liner)
        if is_up_liner:
            up_liner = is_up_liner[0].id
        else:
            up_liner = 0

        User.objects.create(chat_id=chat_id, username=username, first_name=first_name, last_name=last_name,
                            up_liner=up_liner)
        status = True

    return JsonResponse(data={"status": status})


@csrf_exempt
def get_text(request):
    print("req:", request.body)
    body = get_body(request=request)
    key = body.get('key', None)
    response = {
        "text": ""
    }
    if key is not None:
        try:
            txt = Text.objects.get(key=key)
            response["text"] = txt.text
        except Exception as e:
            print(e)

    return JsonResponse(data=response)


def get_text_db(key: str) -> str:
    return Text.objects.get(key=key).text


@csrf_exempt
def get_menu(request):
    body = get_body(request=request)
    key = body.get('key', "")
    print('key:', key)
    if key == 'main_menu_buttons':
        response = {
            "manage_content": get_text_db(key='manage_content'),
            "monetization": get_text_db(key='monetization'),
            "manage_channels": get_text_db(key='manage_channels'),
            "about_as": get_text_db(key='about_as'),
            "support_team": get_text_db(key='support_team')
        }

    elif key == 'all_menu_text_for_message':
        response = {
            "manage_content": get_text_db(key='manage_content'),
            "monetization": get_text_db(key='monetization'),
            "manage_channels": get_text_db(key='manage_channels'),
            "about_as": get_text_db(key='about_as'),
            "support_team": get_text_db(key='support_team'),
            "manage_post_btn": get_text_db(key='manage_post_btn'),
            "manage_subscribe_btn": get_text_db(key='manage_subscribe_btn'),
            "back_main_menu": get_text_db(key='back_main_menu'),
            "schedule_btn": get_text_db(key='schedule_btn'),
            "statistic_btn": get_text_db(key='statistic_btn'),
            "settings_btn": get_text_db(key='settings_btn'),
            "faq_btn": get_text_db(key='faq_btn')
        }

    elif key == 'monitization_menu_buttons':
        response = {
            "schedule_btn": get_text_db(key='schedule_btn'),
            "statistic_btn": get_text_db(key='statistic_btn'),
            "settings_btn": get_text_db(key='settings_btn'),
            "faq_btn": get_text_db(key='faq_btn'),
            "back_main_menu": get_text_db(key='back_main_menu')
        }

    elif key == 'manage_content_menu_buttons':
        response = {
            "manage_post_btn": get_text_db(key='manage_post_btn'),
            "manage_subscribe_btn": get_text_db(key='manage_subscribe_btn'),
            "back_main_menu": get_text_db(key='back_main_menu')
        }

    return JsonResponse(data=response)


def get_channels_of_user(request):
    body = get_body(request=request)
    chat_id = body.get('chat_id', None)
    response = {
        "channels": []
    }
    if chat_id is not None:
        user_instance = User.objects.get(chat_id=chat_id)
        channels_user = Channels.objects.filter(user=user_instance)
        for channel in channels_user:
            response["channels"].append({
                "id": channel.pk,
                "title": channel.title
            })

    return JsonResponse(data=response)
