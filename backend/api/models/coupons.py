from django.db import models


class Coupons(models.Model):
    name = models.CharField(verbose_name='Название купона', max_length=10, default="")
    discount = models.IntegerField(verbose_name='Скидка в %', default=0)
    amount = models.IntegerField(verbose_name='Скидка в руб.', default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Купон'
        verbose_name_plural = 'Купоны'
