from django.db import models


class FAQ(models.Model):
    question = models.CharField(verbose_name='Вопрос', max_length=100, default="")
    answer = models.TextField(verbose_name='Ответ', max_length=200, default="")

    def __str__(self):
        return self.answer

    class Meta:
        verbose_name = "Часто задаваемый вопрос"
        verbose_name_plural = "Часто задаваемые вопросы"
