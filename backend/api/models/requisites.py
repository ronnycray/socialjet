from django.db import models
from .user import User


class Requisites(models.Model):
    user = models.ForeignKey(
        User, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True
    )
    number_cart = models.CharField(verbose_name='Реквизиты', max_length=50, default="")
    describe = models.CharField(verbose_name="Комментарий", max_length=200, default="")

    def __str__(self):
        return self.number_cart

    class Meta:
        verbose_name = 'Реквизиты'
        verbose_name_plural = 'Реквизиты'
