from django.db import models


class Text(models.Model):
    key = models.CharField(verbose_name='Ключ', max_length=100, default="")
    text = models.TextField(verbose_name='Значение', max_length=1000, default="")

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Текст'
        verbose_name_plural = 'Текста'
