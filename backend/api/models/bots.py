from django.db import models
from .user import User


class Bots(models.Model):
    user = models.ForeignKey(
        User, verbose_name="User", on_delete=models.SET_NULL, null=True
    )
    api_token = models.CharField(verbose_name="Token", max_length=300, default="")
    create_at = models.DateTimeField(verbose_name="Create at", auto_now_add=True)

    def __str__(self):
        return self.api_token

    class Meta:
        verbose_name = "Бот"
        verbose_name_plural = "Боты"
