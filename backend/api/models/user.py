from django.db import models


class User(models.Model):
    chat_id = models.CharField(verbose_name="Chat ID", max_length=100, default="")
    username = models.CharField(verbose_name="Username", max_length=100, default="")
    first_name = models.CharField(verbose_name="First Name", max_length=100, default="")
    last_name = models.CharField(verbose_name="Last Name", max_length=100, default="")
    create_at = models.DateTimeField(verbose_name="Create at", auto_now_add=True)
    last_update = models.DateTimeField(verbose_name="Последнее действие", auto_now_add=True)
    up_liner = models.IntegerField(verbose_name="Пригласил", default=0)
    score = models.IntegerField(verbose_name="Очков рейтинга", default=500)
    email = models.CharField(verbose_name="Email", max_length=100, default="")
    token_sj = models.CharField(verbose_name="Токен", max_length=500, default="")

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "Пользвотель"
        verbose_name_plural = "Пользователи"
