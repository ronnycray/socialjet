from django.db import models
from .channels import Channels
from .user import User
from .type_post import TypePost
from django.utils import timezone


class Posts(models.Model):
    channel = models.ForeignKey(
        Channels, verbose_name='Канал', on_delete=models.SET_NULL, null=True
    )
    creator = models.ForeignKey(
        User, verbose_name='Создатель', on_delete=models.SET_NULL, null=True
    )
    content_type = models.CharField(verbose_name='Тип контента', max_length=100, default="text")
    media = models.FileField(verbose_name='Файл', blank=True, upload_to='media/', default="", max_length=1000)
    caption = models.TextField(verbose_name='Текст', max_length=4096, default="", blank=True)
    message_id = models.IntegerField(verbose_name="Message ID", default=0, blank=True)
    type_post = models.ForeignKey(
        TypePost, verbose_name='Тип поста', on_delete=models.SET_NULL, null=True
    )
    notify = models.BooleanField(verbose_name='Уведомление', default=True)
    preview = models.BooleanField(verbose_name='Превью', default=True)
    views = models.IntegerField(verbose_name='Просмотров', default=0, blank=True)
    create_at = models.DateTimeField(verbose_name="Создано в", auto_now_add=True)
    date_send = models.DateTimeField(verbose_name='Отправить в', default=timezone.now, blank=True)
    send = models.BooleanField(verbose_name='Отправлено', default=False)

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
