from django.db import models
from .user import User


class UserSocialJet:
    user = models.ForeignKey(
        User, verbose_name='Пользователь SocialJet', on_delete=models.SET_NULL, null=True
    )
    email = models.CharField(verbose_name="Email", max_length=100, default="")
    password = models.CharField(verbose_name='Пароль', max_length=100, default="")

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = "Пользвотель SocialJet"
        verbose_name_plural = "Пользователи SocialJet"
