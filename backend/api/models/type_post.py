from django.db import models


class TypePost(models.Model):
    name = models.CharField(verbose_name='Тип', default="", max_length=20)
    describe = models.CharField(verbose_name='Описание', default="", max_length=200)
    allowed = models.BooleanField(verbose_name='Доступен', default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Тип поста"
        verbose_name_plural = "Типы постов"
