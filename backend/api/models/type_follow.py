from django.db import models


class TypeFollow(models.Model):
    name = models.CharField(verbose_name="Имя тарифа", max_length=100, default="")
    price = models.IntegerField(verbose_name='Цена', default=0)
    day = models.IntegerField(verbose_name='Дней', default=0)
    discount = models.IntegerField(verbose_name='Скидка в %', default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тариф'
        verbose_name_plural = 'Тарифы'
