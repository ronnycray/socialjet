from django.db import models
from .user import User


class CalendarUser(models.Model):
    user = models.ForeignKey(
        User, verbose_name="Пользователь", on_delete=models.SET_NULL, null=True
    )
    date_send = models.DateTimeField(verbose_name="Дата")
    busy = models.BooleanField(verbose_name='День занят', default=False)
