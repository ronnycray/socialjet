from django.db import models
from .user import User
from .coupons import Coupons


class UserCoupons(models.Model):
    user = models.ForeignKey(
        User, verbose_name='Пользователь', on_delete=models.SET_NULL, null=True
    )
    coupon = models.ForeignKey(
        Coupons, verbose_name='Купон', on_delete=models.SET_NULL, null=True
    )
    active = models.BooleanField(verbose_name='Активный', default=True)

    def __str__(self):
        return self.coupon

    class Meta:
        verbose_name = 'Купон пользователя'
        verbose_name_plural = 'Купоны пользователей'
