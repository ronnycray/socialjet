from django.db import models
from .user import User
from .type_follow import TypeFollow


class Invoices(models.Model):
    user = models.ForeignKey(
        User, verbose_name="Пользователь", on_delete=models.SET_NULL, null=True
    )
    type_follow = models.ForeignKey(
        TypeFollow, verbose_name='Тариф', on_delete=models.SET_NULL, null=True
    )
    buy_at = models.DateTimeField(verbose_name='Куплена', auto_now_add=True)
    date_end_follow = models.DateTimeField(verbose_name='Заканчивается')
    before_day_notify = models.BooleanField(verbose_name='Уведоление за день до кончания', default=False)
    active = models.BooleanField(verbose_name='Активная подписка', default=True)

    def __str__(self):
        return self.type_follow

    class Meta:
        verbose_name = "Подписка"
        verbose_name_plural = "Подписки"
