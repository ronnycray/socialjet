from django.db import models
from .posts import Posts


class Buttons(models.Model):
    post = models.ForeignKey(
        Posts, verbose_name="Пост", on_delete=models.SET_NULL, null=True
    )
    text = models.CharField(verbose_name="Текст", max_length=50, default="")
    url = models.CharField(verbose_name="URL", max_length=500, default="")
    answer = models.TextField(verbose_name="Ответ", max_length=200, default="")
    click = models.IntegerField(verbose_name='Кликов', default=0)

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Кнопка'
        verbose_name_plural = 'Кнопки'
