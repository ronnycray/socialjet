from django.db import models
from .user import User
from .range_field import IntegerRangeField


class Admin(models.Model):
    user = models.ForeignKey(
        User, verbose_name="User", on_delete=models.SET_NULL, null=True
    )
    level = IntegerRangeField(verbose_name="Level", name="level", min_value=1, max_value=3)

    def __str__(self):
        return self.user

    class Meta:
        verbose_name = "Админ"
        verbose_name_plural = "Админы"
