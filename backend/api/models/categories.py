from django.db import models


class Categories(models.Model):
    name = models.CharField(verbose_name='Название категории', max_length=100, default="")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'
