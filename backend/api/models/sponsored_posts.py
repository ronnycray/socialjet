from django.db import models
from .categories import Categories


class SponsoredPosts(models.Model):
    name_post = models.CharField(verbose_name='Имя рекламы', max_length=100, default="")
    caption = models.TextField(verbose_name='Текст', max_length=1024, default="")
    photo = models.ImageField(verbose_name="Картинка", upload_to='media/', default='', blank=True)
    video = models.FileField(verbose_name="Видео", upload_to='media/', default='', blank=True)
    document = models.ImageField(verbose_name="Документ", upload_to='media/', default='', blank=True)
    buttons = models.CharField(verbose_name='Кнопки', max_length=1000, default="")
    views = models.IntegerField(verbose_name='Охват', default=0)
    price = models.IntegerField(verbose_name='Оплата', default=0)
    categories = models.ManyToManyField(
        Categories,
        blank=False
    )
    busy = models.BooleanField(verbose_name='Найден исполнитель', default=False)

    def __str__(self):
        return self.caption

    class Meta:
        verbose_name = 'Рекламный пост SocialJet'
        verbose_name_plural = 'Рекламные посты SocialJet'
