from django.db import models
from .user import User
from .categories import Categories


class Channels(models.Model):
    user = models.ForeignKey(
        User, verbose_name="User", on_delete=models.SET_NULL, null=True
    )
    chat_id = models.CharField(verbose_name="Chat ID", max_length=100, default="")
    chat_id_client = models.IntegerField(verbose_name="Chat ID Client", default=0)
    title = models.CharField(verbose_name="Название", max_length=100, default="")
    followers = models.IntegerField(verbose_name="Подписчиков", default=0)
    join_link = models.CharField(verbose_name="Пригласительная ссылка", max_length=100, default="")
    create_at = models.DateTimeField(verbose_name="Create at", auto_now_add=True)
    categories = models.ForeignKey(
        Categories, verbose_name='Категории канала', on_delete=models.SET_NULL, null=True, blank=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Канал"
        verbose_name_plural = "Каналы"
