from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove
from django.db.models import ObjectDoesNotExist
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
import django
django.setup()
from api.models import (
    Text, Channels, User, Posts
)
import logging

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')


def get_text(key: str) -> str:
    try:
        return Text.objects.get(key=key).text
    except Exception as e:
        logging.warning(f"key: {key}\nerror: {e}")


def make_main_keyboard():
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(get_text(key='manage_content'))
    keyboard.add(get_text(key='monetization'))
    keyboard.add(get_text(key='manage_channels'))
    keyboard.add(get_text(key='about_as'), get_text(key='support_team'))

    return keyboard


def make_keyboard_monitization():
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(get_text(key='schedule_btn'), get_text(key='statistic_btn'))
    keyboard.add(get_text(key='settings_btn'), get_text(key='faq_btn'))
    keyboard.add(get_text(key='back_main_menu'))
    return keyboard


def make_keyboard_manage_content():
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(get_text(key='manage_post_btn'))
    keyboard.add(get_text(key='manage_subscribe_btn'))
    keyboard.add(get_text(key='back_main_menu'))
    return keyboard


def make_keyboard_manage_channels(message):
    keyboard = InlineKeyboardMarkup(row_width=1)
    user = User.objects.get(chat_id=message.chat.id)
    channels = Channels.objects.filter(user=user)
    if len(channels) != 0:
        for channel in channels:
            btn = InlineKeyboardButton(text=channel.title,
                                       callback_data=f'show_info_my_channel-{channel.id}')
            keyboard.add(btn)
    btn = InlineKeyboardButton(text=get_text(key='add_channel_btn'),
                               callback_data='add_channel_user')
    keyboard.add(btn)

    return keyboard


def make_keyboard_manage_post(message):
    keyboard = InlineKeyboardMarkup(row_width=1)
    user = User.objects.get(chat_id=message.chat.id)
    channels = Channels.objects.filter(user=user)
    if len(channels) != 0:
        for channel in channels:
            btn = InlineKeyboardButton(text=channel.title,
                                       callback_data=f'manage_content_my_channel-{channel.id}')
            keyboard.add(btn)
    else:
        btn = InlineKeyboardButton(text=get_text(key='add_channel_btn'),
                                   callback_data='add_channel_user')
        keyboard.add(btn)

    return keyboard


def make_manage_my_channel_keyboard(channel: Channels):
    keyboard = InlineKeyboardMarkup(row_width=1)
    create_post = InlineKeyboardButton(text=get_text(key='text_create_post'),
                                       callback_data=f'create_post_in_channel-{channel.pk}')
    my_posts_in_channel = InlineKeyboardButton(text=get_text(key='text_show_my_posts'),
                                               callback_data=f"show_posts_in_channel-{channel.pk}")
    back = InlineKeyboardButton(text=get_text(key='back'),
                                callback_data=f'back_to_show_my_channels')
    keyboard.add(create_post, my_posts_in_channel, back)

    return keyboard


def make_cancel_keyboard(message):
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(get_text(key='cancel'))

    return keyboard


def make_edit_post_keyboard(post: Posts):
    keyboard = InlineKeyboardMarkup(row_width=2)
    change_text = InlineKeyboardButton(text=get_text(key='change_text'),
                                       callback_data=f'change_data_of_edit_post-change_text_post-{post.id}')
    if post.media == "":
        pin_media = InlineKeyboardButton(text=get_text(key='pin_media'),
                                         callback_data=f'change_data_of_edit_post-pin_media_post-{post.id}')
    else:
        change_media = InlineKeyboardButton(text=get_text(key='change_media'),
                                            callback_data=f'change_data_of_edit_post-change_media_post-{post.id}')
    emotions = InlineKeyboardButton(text=get_text(key='emotions'),
                                    callback_data=f'change_data_of_edit_post-pin_emotions-{post.id}')
    url_btns = InlineKeyboardButton(text=get_text(key='url_btns'),
                                    callback_data=f'change_data_of_edit_post-pin_urls_btns-{post.id}')
    hide_continue = InlineKeyboardButton(text=get_text(key='hide_continue'),
                                         callback_data=f'change_data_of_edit_post-pin_hide_continue-{post.id}')

    if post.notify:
        notify_btn_text = "🔔"
    else:
        notify_btn_text = "🔕"
    notify = InlineKeyboardButton(text=notify_btn_text,
                                  callback_data=f'change_data_of_edit_post-change_notify_status_post-{post.id}')
    if post.preview:
        preview_btn_text = "Превью: Вкл"
    else:
        preview_btn_text = "Превью: Выкл"
    preview = InlineKeyboardButton(text=preview_btn_text,
                                   callback_data=f'change_data_of_edit_post-change_preview_status_post-{post.id}')

    keyboard.add(change_text)
    keyboard.add(pin_media)
    keyboard.add(emotions, url_btns)
    keyboard.add(hide_continue)
    keyboard.add(notify, preview)

    return keyboard


def make_cancel_of_create_post_keyboard():
    keyboard = InlineKeyboardMarkup(row_width=1)
    cancel = InlineKeyboardButton(text=get_text(key='cancel_create_post'),
                                  callback_data='cancel_create_in_channel_post')
    keyboard.add(cancel)

    return keyboard

