import requests
domain = "http://127.0.0.1:8000"


def get_request(name: str, data: dict) -> dict:
    resp = requests.get(url=f"{domain}/{name}/", json=data)
    try:
        response = resp.json()
        return response
    except Exception as e:
        print(e)

    return {}


def is_have_user(chat_id: int) -> bool:
    resp = get_request(name='is_have_user', data={"chat_id": chat_id})
    have = resp.get('have', False)
    if have:
        return True
    return False


def get_up_liner_of_user(chat_id: int) -> int:
    resp = get_request(name='get_up_liner_of_user', data={"chat_id": chat_id})
    chat_id = resp.get('chat_id', False)
    if chat_id:
        return chat_id
    return 0


def create_user(chat_id: int, username: str, first_name: str, last_name: str, up_liner: int) -> bool:
    resp = get_request(name='create_user', data={"chat_id": chat_id,
                                                 "username": username,
                                                 "first_name": first_name,
                                                 "last_name": last_name,
                                                 "up_liner": up_liner})
    status = resp.get('status', False)
    return status


def api_get_text(key: str) -> str:
    resp = get_request(name='get_text', data={"key": key})
    text = resp.get('text', False)
    if text:
        return text
    return ""


def api_get_menu(key: str) -> dict:
    resp = get_request(name='get_menu', data={"key": key})
    print(resp)
    return resp


def get_channels_of_user(chat_id: int) -> list:
    resp = get_request(name='get_channels_of_user', data={"chat_id": chat_id})
    channels = resp.get('channels', [])
    return channels
