from django.db.models import ObjectDoesNotExist
from telebot.apihelper import ApiException
import logging
import telebot
import json
import re
import os

from api import (
    api_get_menu,
    api_get_text,
    is_have_user,
    get_up_liner_of_user,
    create_user,
    get_channels_of_user
)
from keyboards import (
    make_main_keyboard,
    make_keyboard_monitization,
    make_keyboard_manage_content,
    make_keyboard_manage_channels,
    make_keyboard_manage_post
)

# Подключение конфигурационного файла
with open('config/config.json', 'r', encoding='utf-8') as lib:
    data = json.load(lib)


limit_simple_text = 4096
limit_caption_media = 1024
limit_size_file_send = 50
limit_size_file_received = 20
limit_text_on_button = 200

content_type_message = 'text'
content_type_photo = 'photo'
content_type_video = 'video'
content_type_audio = 'audio'
content_type_document = 'document'

token = data['token']
bot = telebot.TeleBot(token)

PATH_MAIN_FILE_PHOTO = data['PATH_MAIN_FILE_PHOTO']


def get_text(key: str) -> str:
    try:
        return api_get_text(key=key)
    except Exception as e:
        logging.warning(f"key: {key}\nerror: {e}")


def get_menu(key: str) -> dict:
    try:
        return api_get_menu(key=key)
    except Exception as e:
        logging.warning(f"key: {key}\nerror: {e}")


@bot.message_handler(commands=['start'])
def get_start(message):
    print(message)
    chat_id = int(message.chat.id)

    if not is_have_user(chat_id=chat_id):
        print('chat_id:', chat_id)
        username = message.from_user.username if message.from_user.username else ""
        first_name = message.from_user.first_name if message.from_user.first_name else ""
        last_name = message.from_user.last_name if message.from_user.last_name else ""
        up_liner = 0

        if ' ' in message.text:
            user_up_liner = message.text.split('/start ')[1]
            if user_up_liner.isdigit():
                up_liner = user_up_liner

        create_user(chat_id=chat_id, username=username, first_name=first_name, last_name=last_name, up_liner=up_liner)

    bot.send_photo(chat_id=chat_id,
                   caption=get_text(key='main_text_message'),
                   parse_mode={'HTML'},
                   photo=open(PATH_MAIN_FILE_PHOTO, 'rb'),
                   reply_markup=make_main_keyboard())


@bot.message_handler(content_types='text')
def define_message(message):
    chat_id = message.chat.id
    all_menu_text = get_menu(key='all_menu_text_for_message')
    print(message)

    if message.text == all_menu_text['manage_content']:
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='manage_content_text_menu'),
                         parse_mode={'HTML'},
                         reply_markup=make_keyboard_manage_content())

    elif message.text == all_menu_text['monetization']:
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='monitization_text_menu'),
                         parse_mode={'HTML'},
                         reply_markup=make_keyboard_monitization())

    elif message.text == all_menu_text['manage_channels']:
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='manage_channels_text_menu'),
                         parse_mode={'HTML'},
                         reply_markup=make_keyboard_manage_channels(message=message))

    elif message.text == all_menu_text['about_as']:
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='about_us_text_menu'),
                         parse_mode={'HTML'})

    elif message.text == all_menu_text['support_team']:
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='support_team_text_menu'),
                         parse_mode={'HTML'})

    elif message.text == all_menu_text['back_main_menu']:
        bot.send_message(chat_id=chat_id,
                         text=get_text(key='main_menu_text'),
                         parse_mode={'HTML'},
                         reply_markup=make_main_keyboard())

    elif message.text == all_menu_text['manage_post_btn']:
        channels = get_channels_of_user(chat_id=message.chat.id)
        if len(channels) != 0:
            bot.send_message(chat_id=chat_id,
                             text=get_text(key='manage_post_btn_text_menu'),
                             parse_mode={'HTML'},
                             reply_markup=make_keyboard_manage_post(message=message))
        else:
            bot.send_message(chat_id=chat_id,
                             text=get_text(key='not_have_channel'),
                             parse_mode={'HTML'},
                             reply_markup=make_keyboard_manage_post(message=message))

    elif message.text == all_menu_text['manage_subscribe_btn']:
        pass

    elif message.text == all_menu_text['schedule_btn']:
        pass

    elif message.text == all_menu_text['statistic_btn']:
        pass

    elif message.text == all_menu_text['settings_btn']:
        pass

    elif message.text == all_menu_text['faq_btn']:
        pass


@bot.callback_query_handler(func=lambda call_back: True)
def define_callback(call_back):
    print(call_back.data)
    chat_id = int(call_back.message.chat.id)
    message = call_back.message
    bot.clear_step_handler(message=message)

    if 'show_info_my_channel' in call_back.data:
        pass

    elif 'add_channel_user' == call_back.data:
        pass

    elif 'show_my_posts_channel' in call_back.data:
        pass


bot.polling(none_stop=True)
