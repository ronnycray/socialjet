from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup, ReplyKeyboardRemove
from django.db.models import ObjectDoesNotExist
from api import (
    api_get_menu,
    api_get_text,
    get_channels_of_user
)
import logging

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')


def get_text(key: str) -> str:
    try:
        return api_get_text(key=key)
    except Exception as e:
        logging.warning(f"key: {key}\nerror: {e}")


def get_menu(key: str) -> dict:
    try:
        return api_get_menu(key=key)
    except Exception as e:
        logging.warning(f"key: {key}\nerror: {e}")


def make_main_keyboard():
    menu_buttons = get_menu(key='main_menu_buttons')
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(menu_buttons['manage_content'])
    keyboard.add(menu_buttons['monetization'])
    keyboard.add(menu_buttons['manage_channels'])
    keyboard.add(menu_buttons['about_as'], menu_buttons['support_team'])

    return keyboard


def make_keyboard_monitization():
    menu_buttons = get_menu(key='monitization_menu_buttons')
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(menu_buttons['schedule_btn'], menu_buttons['statistic_btn'])
    keyboard.add(menu_buttons['settings_btn'], menu_buttons['faq_btn'])
    keyboard.add(menu_buttons['back_main_menu'])
    return keyboard


def make_keyboard_manage_content():
    menu_buttons = get_menu(key='manage_content_menu_buttons')
    keyboard = ReplyKeyboardMarkup(resize_keyboard=True)
    keyboard.add(menu_buttons['manage_post_btn'])
    keyboard.add(menu_buttons['manage_subscribe_btn'])
    keyboard.add(menu_buttons['back_main_menu'])
    return keyboard


def make_keyboard_manage_channels(message):
    keyboard = InlineKeyboardMarkup(row_width=1)
    channels = get_channels_of_user(chat_id=message.chat.id)
    if len(channels) != 0:
        for channel in channels:
            btn = InlineKeyboardButton(text=channel['title'],
                                       callback_data=f'show_info_my_channel-{channel["id"]}')
            keyboard.add(btn)
    btn = InlineKeyboardButton(text=get_text(key='add_channel_btn'),
                               callback_data='add_channel_user')
    keyboard.add(btn)

    return keyboard


def make_keyboard_manage_post(message):
    keyboard = InlineKeyboardMarkup(row_width=1)
    channels = get_channels_of_user(chat_id=message.chat.id)
    if len(channels) != 0:
        for channel in channels:
            btn = InlineKeyboardButton(text=channel['title'],
                                       callback_data=f'show_my_posts_channel-{channel["id"]}')
            keyboard.add(btn)
    else:
        btn = InlineKeyboardButton(text=get_text(key='add_channel_btn'),
                                   callback_data='add_channel_user')
        keyboard.add(btn)

    return keyboard
